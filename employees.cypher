// pracownicy 
CREATE (e1:Employee {name: "John", surname: "Doe", position: "developer"})
CREATE (e2:Employee {name: "Jack", surname: "Black", position: "engineer"})
CREATE (e3:Employee {name: 'Percy', surname: 'Weasley', position: 'designer'})
CREATE (e4:Employee {name: 'Alice', surname: 'Smith', position: 'administrator'})
CREATE (e5:Employee {name: 'Mike', surname: 'Johnson', position: 'developer'})
CREATE (e6:Employee {name: 'Emily', surname: 'Brown', position: 'manager'})
CREATE (e7:Employee {name: 'Sarah', surname: 'Wilson', position: 'consultant'})
CREATE (e8:Employee {name: 'David', surname: 'Lee', position: 'coordinator'})
CREATE (e9:Employee {name: 'Jennifer', surname: 'Garcia', position: 'representative'})
CREATE (e10:Employee {name: 'Daniel', surname: 'Martinez', position: 'supervisor'})
CREATE (e11:Employee {name: 'Olivia', surname: 'Lopez', position: 'consultant'})
CREATE (e12:Employee {name: 'William', surname: 'Hernandez', position: 'manager'})
CREATE (e13:Employee {name: 'Sophia', surname: 'Gonzalez', position: 'analyst'})
CREATE (e14:Employee {name: 'James', surname: 'Perez', position: 'analyst'})
CREATE (e15:Employee {name: 'Emma', surname: 'Torres', position: 'specialist'})
CREATE (e16:Employee {name: 'Michael', surname: 'Moore', position: 'strategist'})
CREATE (e17:Employee {name: 'Ava', surname: 'Miller', position: 'content writer'})
CREATE (e18:Employee {name: 'Alexander', surname: 'Sanchez', position: 'manager'})
CREATE (e19:Employee {name: 'Mia', surname: 'Rossi', position: 'auditor'})
CREATE (e20:Employee {name: 'Benjamin', surname: 'Russell', position: 'accountant'})
CREATE (e21:Employee {name: 'Charlotte', surname: 'Gomez', position: 'assistant'})
CREATE (e22:Employee {name: 'Elijah', surname: 'King', position: 'controller'})
CREATE (e23:Employee {name: 'Amelia', surname: 'Watson', position: 'representative'})
CREATE (e24:Employee {name: 'Lucas', surname: 'Ward', position: 'manager'})
CREATE (e25:Employee {name: 'Madison', surname: 'Diaz', position: 'operator'})
CREATE (e26:Employee {name: 'Harper', surname: 'Murphy', position: 'operator'})
CREATE (e27:Employee {name: 'Evelyn', surname: 'Cook', position: 'planner'})
CREATE (e28:Employee {name: 'Logan', surname: 'Bailey', position: 'supervisor'})
CREATE (e29:Employee {name: 'Liam', surname: 'Rivera', position: 'operator'})
CREATE (e30:Employee {name: 'Aiden', surname: 'Ward', position: 'manager'})

// departamenty
CREATE (d1:Department {department_name: 'IT'})
CREATE (d2:Department {department_name: 'HR'})
CREATE (d3:Department {department_name: 'Marketing'})
CREATE (d4:Department {department_name: 'Finance'})
CREATE (d5:Department {department_name: 'Production'})

// ustawienie relacji WORKS_IN
CREATE
  (e1) - [:WORKS_IN] -> (d1),
  (e2) - [:WORKS_IN] -> (d1),
  (e3) - [:WORKS_IN] -> (d1),
  (e4) - [:WORKS_IN] -> (d1),
  (e5) - [:WORKS_IN] -> (d1),
  (e6) - [:WORKS_IN] -> (d1)

CREATE
  (e7) - [:WORKS_IN] -> (d2),
  (e8) - [:WORKS_IN] -> (d2),
  (e9) - [:WORKS_IN] -> (d2),
  (e10) - [:WORKS_IN] -> (d2),
  (e11) - [:WORKS_IN] -> (d2),
  (e12) - [:WORKS_IN] -> (d2)

CREATE
  (e13) - [:WORKS_IN] -> (d3),
  (e14) - [:WORKS_IN] -> (d3),
  (e15) - [:WORKS_IN] -> (d3),
  (e16) - [:WORKS_IN] -> (d3),
  (e17) - [:WORKS_IN] -> (d3),
  (e18) - [:WORKS_IN] -> (d3)

CREATE
  (e19) - [:WORKS_IN] -> (d4),
  (e20) - [:WORKS_IN] -> (d4),
  (e21) - [:WORKS_IN] -> (d4),
  (e22) - [:WORKS_IN] -> (d4),
  (e23) - [:WORKS_IN] -> (d4),
  (e24) - [:WORKS_IN] -> (d4)

CREATE
  (e25) - [:WORKS_IN] -> (d5),
  (e26) - [:WORKS_IN] -> (d5),
  (e27) - [:WORKS_IN] -> (d5),
  (e28) - [:WORKS_IN] -> (d5),
  (e29) - [:WORKS_IN] -> (d5),
  (e30) - [:WORKS_IN] -> (d5)

// relacja MANAGES

FOREACH (emp IN [e1, e2, e3, e4, e5] | 
  MERGE (e6) - [:MANAGES] -> (emp)
)

FOREACH (emp IN [e7, e8, e9, e10, e11] | 
  MERGE (e12) - [:MANAGES] -> (emp)
)

FOREACH (emp IN [e13, e14, e15, e16, e17] | 
  MERGE (e18) - [:MANAGES] -> (emp)
)

FOREACH (emp IN [e19, e20, e21, e22, e23] | 
  MERGE (e24) - [:MANAGES] -> (emp)
)

FOREACH (emp IN [e25, e26, e27, e28, e29] | 
  MERGE (e30) - [:MANAGES] -> (emp)
)