# REST API Flask + Neo4j
Projekt **Zadania 2** zawiera aplikację rejestru pracowników. Jest stworzony we Flasku i połączony z bazą danych Neo4j. Aplikacja jest **asynchroniczna** i zawiera endpointy do każdego podpunktu z polecenia.

Plik `app.py` zawiera całą logikę i serwer, a `employees.cypher` dane, które są wprowadzone do bazy.